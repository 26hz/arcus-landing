if($("#submit-form").length < 0 ){
	document.querySelector('#submit-form').addEventListener('submit',async function (event) {
		event.preventDefault();
		const form = event.target;
		const res = await fetch("/signup", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				userName: form.userName.value,
				userEmail: form.userEmail.value
			}),
		});
		let result = await res.json();
			document.querySelector("#regMsg").innerHTML = ``;
		if (res.status === 200 && result.success) {
			document.querySelector("#regMsg").innerHTML = result.message;
			$('.thanks, .login').toggleClass('d-block d-none');
			return;
		} else if (res.status === 202) {
			document.querySelector("#regMsg").innerHTML = result.message;
			$('.thanks, .login').toggleClass('d-block d-none');
			return;
		}
		else {
			window.location="./404.html"
		}
	});
}

if($("#submit-form-promo").length < 0 ){
	document.querySelector('#submit-form-promo').addEventListener('submit',async function (event) {
		event.preventDefault();
		const form = event.target;
		const res = await fetch("/promoSignup", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				userName: form.userName.value,
				userEmail: form.userEmail.value
			}),
		});
		let result = await res.json();
			document.querySelector("#regMsg").innerHTML = ``;
		if (res.status === 200 && result.success) {
			document.querySelector("#regMsg").innerHTML = result.message;
			$('.thanks, .offer').toggleClass('d-block d-none');
			return;
		} else if (res.status === 202) {
			document.querySelector("#regMsg").innerHTML = result.message;
			$('.thanks, .offer').toggleClass('d-block d-none');
			return;
		}
		else {
			window.location="./404.html"
		}
	});
}

if($("#submit-form-inquiry").length < 0 ){
	document.querySelector('#submit-form-inquiry').addEventListener('submit',async function (event) {
		event.preventDefault();
		const form = event.target;
		const res = await fetch("/inquiry", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				userName: form.userName.value,
				userEmail: form.userEmail.value,
				content: form.content.value
			}),
		});
		let result = await res.json();
		if (res.status === 200 && result.success) {
			$('.thanks, .contact').toggleClass('d-none d-block')
			return;
		}
		else {
			console.log("ERROR")		
		}
	});
}

$(function() {
	var userAgent = window.navigator.userAgent.toLowerCase(),
		ios = /iphone|ipod|ipad/.test(userAgent),
		android = /android/.test(userAgent);

	$.each($(".accordion-container"), function() {
    $(this).click(function() {
			//inactive all others accordion in the div
			var allAccordion = $(this).parent().children();
			var allAccordionPanel = $(this).parent().children().find(".panel");
			var allAccordionIcon = $(this).parent().children().find(".accordion-icon img");
			allAccordion.removeClass("active");
			allAccordionPanel.css('max-height', "0px");
			allAccordionIcon.attr("src","assets/img/icons/down.webp");
			
			var panel = $(this).find(".panel");
			var icon = $(this).find(".accordion-icon img");
			var panelMaxHeight = panel.css('max-height');
			if (panelMaxHeight && panelMaxHeight != "0px") {
				panel.css('max-height', "0px");
				icon.attr("src","assets/img/icons/down.webp");
			} else {
				$(this).addClass("active");
				panel.css('max-height', (panel[0].scrollHeight+22)+"px");
				icon.attr("src","assets/img/icons/up.webp");
			}
			
			var parentClass = $(this).parent().attr("class");
			if(parentClass.includes("solution-list")){
				var image = $(this).data("image")
				$('.solution-image').attr("src","assets/img/solution/"+image+".webp");
			} else if(parentClass.includes("instruction-list")){
				var video = $(this).data("video");
				var videoTag = $('.instruction-video');
				var containerTag = $('.instruction-video-container');
				//move container video and gif to below accordion in mobile
				if(android || ios){
					$(this).after(containerTag);
				}
				//if it's clicked double to minimize the video is also hidden
				if($(this).data("first")){
					$(this).data("first") = false;
				} else if(videoTag.data("video") == video) {
					containerTag.hide();
				} else {
					containerTag.show();
					videoTag.hide();
					videoTag.data("video", video);
					videoTag.attr("src","assets/video/instruction/Step"+video+".mp4");
					$(videoTag).on("canplay", () => {
						videoTag.show();
					});
				}
			}
    });
	});


	$.each($(".accordion-pricing"), function() {
		$(this).click(function() {
			var panel = $(this).children(".panel");
			var icon = $(this).children(".accordion-icon img");
			var panelMaxHeight = panel.css('max-height');
			if (panelMaxHeight && panelMaxHeight != "0px") {
				panel.css('max-height', "0px");
				icon.attr("src","assets/img/icons/down-tosca.webp");
			} else {
				panel.css('max-height', (panel[0].scrollHeight+22)+"px");
				icon.attr("src","assets/img/icons/up-tosca.webp");
			}
		});
	});

	//faq accordion
	$.each($(".faq-accordion-container"), function() {
    $(this).click(function() {
			//inactive all others accordion in the div
			var allAccordion = $(this).parent().children();
			var allAccordionPanel = $(this).parent().children().find(".panel");
			var allAccordionIcon = $(this).parent().children().find(".accordion-icon img");
			allAccordion.removeClass("active");
			allAccordionPanel.css('max-height', "0px");
			allAccordionIcon.attr("src","assets/img/icons/down-black.webp");
			
			var panel = $(this).find(".panel");
			var icon = $(this).find(".accordion-icon img");
			var panelMaxHeight = panel.css('max-height');
			if (panelMaxHeight && panelMaxHeight != "0px") {
				panel.css('max-height', "0px");
				icon.attr("src","assets/img/icons/down-black.webp");
			} else {
				$(this).addClass("active");
				panel.css('max-height', (panel[0].scrollHeight+22)+"px");
				icon.attr("src","assets/img/icons/up-black.webp");
			}
    });
	});

	//modal triggers action
	$('.login-modal-trigger, .container .btn-primary, .subscription-button').click(function() {
		$('#myModal, .login').toggleClass('d-none d-block');
		if($('.thanks, .contact').hasClass("d-block")){
			$('.thanks, .contact').removeClass("d-block").addClass('d-none');
		}
	});

	//so the modal isn't closed when we clicked the content
	$('.modal-content').click(function() {
		event.stopPropagation();
	});

	$('.modal .close, .modal').click(function() {
		$('#myModal, .contact, .login, .thanks, .offer, .offer-thanks').removeClass('d-block').addClass('d-none');
	});


	$('.contact-trigger, .btn-package-action').click(function() {
		$('.thanks, .login, .offer, .offer-thanks').removeClass("d-block").addClass('d-none');
		$('#myModal, .contact').removeClass('d-none').addClass("d-block");
	});

	$('.bars-icon, .nav-container .close').click(function() {
		$('.nav-container').toggleClass('d-none d-flex')
	});

	$('.pricing-bars-icon, .nav-pricing-container .close').click(function() {
		$('.nav-pricing-container').toggleClass('d-none d-block')
	});

	setTimeout(function(){
		$('.contact, .login, .thanks').removeClass('d-block').addClass('d-none');
		$('#myModal, .offer').addClass('d-block').removeClass('d-none');
	}, 30000);

	// desktop ar-button action
	if(!android && !ios){
		$('.ar-button-desktop').css('display', 'flex');
		$('.ar-button-desktop').click(function(e) {
			$('.popup-ar-desktop').css('display', 'block');
		});
	}

	$('.popup-close').click(function(){
		$('.popup-ar-desktop').hide();
	})

	$('.out-of-bound-ar-button').click(function(){
		$('#ar-button').click();
	})
});


