CREATE TABLE users(
    id SERIAL primary key,
    name VARCHAR(255),
    email VARCHAR(255),
    created_at TIMESTAMP
);

CREATE TABLE inquiry(
    id SERIAL primary key,
    name VARCHAR(255),
    email VARCHAR(255),
    content TEXT,
    created_at TIMESTAMP
);

CREATE TABLE users_promo(
    id SERIAL primary key,
    name VARCHAR(255),
    email VARCHAR(255),
    created_at TIMESTAMP
);