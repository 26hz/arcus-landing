import express from "express";
import expressSession from "express-session";
import path from "path";
import bodyParser from "body-parser";
import dotenv from "dotenv";
import pg from "pg";

dotenv.config()
const app = express();

export const client = new pg.Client({
  user:process.env.DB_USERNAME,
  password:process.env.DB_PASSWORD,
  database:process.env.DB_NAME,
  host:process.env.DB_HOST,
  port:5432
})

client.connect();

app.use(express.static("public"));


app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.use(
  expressSession({
    secret: "arcuslanding",
    resave: true,
    saveUninitialized: true,
  })
);

app.post("/promoSignup", async (req, res) => {
  try {
    const userName = req.body.userName;
    const userEmail = req.body.userEmail;
    //check if there is any duplicate application
    let checkResult = await client.query(
      /*sql*/`SELECT * FROM users_promo WHERE email = $1`,
      [userEmail]
    );

    if (checkResult.rowCount === 0) {
      await client.query(
        /*sql*/`INSERT INTO users_promo (name, email, created_at) VALUES ($1, $2, NOW());`,
        [userName, userEmail]
      );
      return res.status(200).json({
        success: true,
        message: "You have registered for the early access!"
      });
    } else if (checkResult.rowCount === 1) {
      return res.status(202).json({
        message: "You have already signed up"
      });
    }
  } catch (err) {
    return res.status(500).json({err})
  }
  return
});

app.post("/inquiry", async (req, res) => {
  try {
    const userName = req.body.userName;
    const userEmail = req.body.userEmail;
    const content = req.body.content;

      await client.query(
        /*sql*/`INSERT INTO inquiry (name, email, content, created_at) VALUES ($1, $2, $3, NOW());`,
        [userName, userEmail, content]
      );
      
      return res.status(200).json({
        success: true
    });
    
  } catch (err) {
    return res.status(500).json({err})
  }
  return
});

app.post("/signup", async (req, res) => {
  try {
    const userName = req.body.userName;
    const userEmail = req.body.userEmail;
    //check if there is any duplicate application
    let checkResult = await client.query(
      /*sql*/`SELECT * FROM users WHERE email = $1`,
      [userEmail]
    );

    if (checkResult.rowCount === 0) {
      await client.query(
        /*sql*/`INSERT INTO users (name, email, created_at) VALUES ($1, $2, NOW());`,
        [userName, userEmail]
      );
      return res.status(200).json({
        success: true,
        message: "You have registered for the early access!"
            });
    } else if (checkResult.rowCount === 1) {
      return res.status(202).json({
        message: "You have already signed up!"
      });
    }
  } catch (err) {
    return res.status(500).json({err})
  }
  return
});


app.listen(process.env.PORT  || 3000, () => {
  console.log(`Server listening${process.env.PORT}`)
})

app.use((req, res) => {
  res.sendFile(path.join(__dirname, "./public/404.html"));
});